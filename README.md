# @manuscripts/csl-styles

The data in this repository is derived from the [CSL Style Repository](https://github.com/citation-style-language/styles).

# Updating

To update styles run `git submodule update --remote` then `npm run build`.

# Licensing

All the styles in this repository are released under the [Creative Commons Attribution-ShareAlike 3.0 Unported license](http://creativecommons.org/licenses/by-sa/3.0/). For attribution, any software using CSL styles from this repository must include a clear mention of the CSL project and a link to [CitationStyles.org](http://citationstyles.org/). When distributing these styles, the listings of authors and contributors in the style metadata must be kept as is.
